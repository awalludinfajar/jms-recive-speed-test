package com.multithread.sample.configuration;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.tibco.tibjms.TibjmsConnectionFactory;

@Configuration
public class TibcoConnectionConfiguration {
    
    @Value("${ems.port}")
	private String port;

	@Value("${ems.server}")
	private String server;

	@Value("${ems.user}")
	private String user;

    @Value("${ems.queue}")
    private String queueName;

    public ConnectionFactory getConnectionFactory() throws JMSException {
        final TibjmsConnectionFactory connection = new TibjmsConnectionFactory();
        connection.setServerUrl("tcp://" + server + ":" + port);
        connection.setUserName(user);
        connection.setUserPassword("");
        return connection;
    }
}
