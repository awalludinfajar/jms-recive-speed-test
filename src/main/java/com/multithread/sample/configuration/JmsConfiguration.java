package com.multithread.sample.configuration;

import javax.jms.JMSException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableJms
public class JmsConfiguration {
    
    @Autowired
    private TibcoConnectionConfiguration factoryConfiguration;

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor threadPoolExecutor = new ThreadPoolTaskExecutor();
        threadPoolExecutor.setCorePoolSize(5);
        // threadPoolExecutor.setMaxPoolSize(2);
        // threadPoolExecutor.setQueueCapacity(5);
        threadPoolExecutor.setThreadNamePrefix("MyExecutor-");
        threadPoolExecutor.initialize();
        return threadPoolExecutor;
    }

    @Bean
    public JmsListenerContainerFactory<?> jmsListenerContainerFactory() throws JMSException {
        DefaultJmsListenerContainerFactory listenerContainerFactory = new DefaultJmsListenerContainerFactory();
        listenerContainerFactory.setConnectionFactory(factoryConfiguration.getConnectionFactory());
        listenerContainerFactory.setTaskExecutor(taskExecutor());
        // listenerContainerFactory.setConcurrency("2-5");
        return listenerContainerFactory;
    }
}
