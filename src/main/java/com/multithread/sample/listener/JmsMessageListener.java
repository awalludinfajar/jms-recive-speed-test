package com.multithread.sample.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.multithread.sample.model.SendMessage;

@Component
public class JmsMessageListener {
    
    private final List<String> message = new ArrayList<>();

    public StopWatch mStopWatch = new StopWatch();

    @Autowired
    private ObjectMapper mapper;

    private final int expectedMessageCount = 100;

    @JmsListener(destination = "test.queue")
    public void reciveMessage(String text) throws JsonMappingException, JsonProcessingException {
        synchronized (this) {
            if (!mStopWatch.isRunning()) {
                mStopWatch.start();
            }
            message.add(text);
            System.out.println(collectData(text));

            if (message.size() >= expectedMessageCount) {
                mStopWatch.stop();
                System.out.println("Total processing time: " + (long) mStopWatch.getTotalTime(TimeUnit.SECONDS) + " seconds.");
            }
        }
    }

    public String collectData(String data) throws JsonMappingException, JsonProcessingException {
        SendMessage msg = mapper.readValue(data, SendMessage.class);
        String result = "Name : " + msg.getName() +
                        "\nAddress : " + msg.getAddress() +
                        "\nIteration : " + msg.getIterations() +
                        "\n"+ Thread.currentThread().getName()+"\n";
        return result;
    }
}
